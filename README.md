# uTravel webapp - create Your journey

##### In order to run application you need to:
1. Run dev database with Docker (recommended):
```
docker run --name utravel -e POSTGRES_PASSWORD=root -e POSTGRES_DB=utravel -d -p 5432:5432 postgres:13.0-alpine
```
2. Set `application.properties`
3. https://spring.io/guides/gs/maven/