package com.app.utravel.services;

import com.app.utravel.models.News;

import java.util.List;
import java.util.Optional;

public interface INewsService {

    List<News> findAll();
    void save(News news);
    void deleteById(Long id);
    Optional<News> findById(Long id);
    List<News> findByTitleContaining(String title);
    List<News> findByType(String type);
    List<News> findByAttractionId(Long id);
    List<News> findByAttractionsNameContaining(String name);
    List<News> findByAttractionsType(String type);
}
