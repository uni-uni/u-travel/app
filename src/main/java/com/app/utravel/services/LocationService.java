package com.app.utravel.services;

import com.app.utravel.models.Location;
import com.app.utravel.repositories.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LocationService implements ILocationService {

    @Autowired
    private LocationRepository locationRepository;

    @Override
    public List<Location> findAll() {
        return locationRepository.findAll();
    }

    @Override
    public void save(Location location) {
        locationRepository.save(location);
    }

    @Override
    public void deleteById(Long id) {
        locationRepository.deleteById(id);
    }

    @Override
    public Optional<Location> findById(Long id) {
        return locationRepository.findById(id);
    }

    @Override
    public List<Location> findByCityContaining(String city) {
        return locationRepository.findByCityContaining(city);
    }

    @Override
    public List<Location> findByStreetContaining(String street) {
        return locationRepository.findByStreetContaining(street);
    }

    @Override
    public List<Location> findByPostalCodeContaining(String postalCode) {
        return locationRepository.findByPostalCodeContaining(postalCode);
    }

    @Override
    public List<Location> findByAttractionId(Long id) {
        return locationRepository.findByAttractionsId(id);
    }

    @Override
    public List<Location> findByAttractionNameContaining(String name) {
        return locationRepository.findByAttractionsNameContaining(name);
    }
}
