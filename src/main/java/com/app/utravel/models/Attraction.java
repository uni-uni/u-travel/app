package com.app.utravel.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "attraction")
public class Attraction {

    @Id
    @GeneratedValue
    private Long id;

    @NotBlank
    @Column(unique = true)
    private String name;

    @NotBlank
    private String type;

    private String imgSrc;

    @ManyToMany(targetEntity = Location.class, fetch = FetchType.LAZY)
    private List<Location> locations;

    @ManyToMany(targetEntity = News.class, fetch = FetchType.LAZY)
    private List<News> news;

    @ManyToMany(targetEntity = User.class, fetch = FetchType.LAZY)
    private List<User> usersWhoLikeIt;
}
