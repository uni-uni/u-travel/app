package com.app.utravel.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "location",
        uniqueConstraints =
                @UniqueConstraint(columnNames = {"longitude", "latitude"})
)
public class Location {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String city;

    @NotNull
    private String postalCode;

    @NotNull
    private String street;

    @NotNull
    private String streetNo;

    @NotBlank
    private String longitude;

    @NotBlank
    private String latitude;

    @ManyToMany(mappedBy = "locations", targetEntity = Attraction.class, fetch = FetchType.LAZY)
    private List<Attraction> attractions;
}
