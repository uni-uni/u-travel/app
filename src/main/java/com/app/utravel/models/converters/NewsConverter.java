package com.app.utravel.models.converters;

import com.app.utravel.models.News;
import com.app.utravel.services.INewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class NewsConverter implements Converter<Long, News> {

    @Autowired
    INewsService newsService;

    @Override
    public News convert(Long id) {
        return newsService.findById(id).get();
    }
}
