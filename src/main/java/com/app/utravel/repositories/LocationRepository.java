package com.app.utravel.repositories;

import com.app.utravel.models.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {

    List<Location> findByCityContaining(String city);
    List<Location> findByStreetContaining(String street);
    List<Location> findByPostalCodeContaining(String postalCode);
    List<Location> findByAttractionsId(Long Id);
    List<Location> findByAttractionsNameContaining(String name);
}
