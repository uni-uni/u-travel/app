package com.app.utravel.controllers;

import com.app.utravel.models.Attraction;
import com.app.utravel.models.Location;
import com.app.utravel.models.News;
import com.app.utravel.models.User;
import com.app.utravel.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
public class AdminController {

    @Autowired
    private IAttractionService attractionService;

    @Autowired
    private ILocationService locationService;

    @Autowired
    private INewsService newsService;

    @Autowired
    private IUserService userService;

    @GetMapping("/admin")
    public String showPanel() {
        return "administration_panel";
    }

    @GetMapping("/admin/attractions/add")
    public String showAttractionAddPanel(Model model) {
        setModelAttributes(model);
        model.addAttribute("viewType", "attractions-add");
        model.addAttribute("attraction", new Attraction());

        return "administration_panel";
    }

    @PostMapping("/admin/attractions/add/save")
    public String saveAttraction(@ModelAttribute Attraction attraction,
                                 HttpServletRequest request,
                                 Model model) {
        attractionService.save(attraction);

        String referer = request.getHeader("Referer");
        return "redirect:" + referer;
    }

    @GetMapping("/admin/attractions/search")
    public String getAttractionsToEdit(@RequestParam(value="name", required=false, defaultValue = "") String name,
                                   Model model) {
        List<Attraction> attractions = attractionService.findByNameContaining(name);
        model.addAttribute("attractions", attractions);
        model.addAttribute("attraction", new Attraction());
        model.addAttribute("viewType", "attractions-search");
        return "administration_panel";
    }

    @GetMapping("/admin/attractions/search/edit")
    public String showAttractionToEdit(@RequestParam(value="id", required=false) Long id,
                                 Model model) {
        Optional<Attraction> attraction = attractionService.findById(id);
        model.addAttribute("attraction", attraction.get());
        model.addAttribute("allLocations", locationService.findAll());
        model.addAttribute("allNews", newsService.findAll());
        model.addAttribute("viewType", "attractions-edit");
        return "administration_panel";
    }

    @GetMapping("/admin/locations/add")
    public String showLocationAddPanel(Model model) {
        setModelAttributes(model);
        model.addAttribute("viewType", "locations-add");
        model.addAttribute("location", new Location());
        return "administration_panel";
    }

    @PostMapping("/admin/locations/add/save")
    public String saveLocation(@ModelAttribute Location location,
                               HttpServletRequest request,
                               Model model) {
        locationService.save(location);

        String referer = request.getHeader("Referer");
        return "redirect:" + referer;
    }

    @GetMapping("/admin/locations/search")
    public String getLocationsToEdit(@RequestParam(value="city", required=false, defaultValue = "") String city,
                                       @RequestParam(value="postalCode", required=false, defaultValue = "") String postalCode,
                                       Model model) {
        List<Location> locations;
        if (! city.isEmpty()) {
            locations = locationService.findByCityContaining(city);
            locations = ! postalCode.isEmpty()
                    ? locations.stream()
                        .filter(attraction -> postalCode.equals(attraction.getPostalCode()))
                        .collect(Collectors.toList())
                    : locations;
        } else {
            locations = locationService.findByPostalCodeContaining(postalCode);
        }
        model.addAttribute("locations", locations);
        model.addAttribute("location", new Location());
        model.addAttribute("viewType", "locations-search");
        return "administration_panel";
    }

    @GetMapping("/admin/locations/search/edit")
    public String showLocationToEdit(@RequestParam(value="id", required=false) Long id,
                                       Model model) {
        Optional<Location> location = locationService.findById(id);
        model.addAttribute("location", location.get());
        model.addAttribute("viewType", "locations-edit");
        return "administration_panel";
    }

    @GetMapping("/admin/news/add")
    public String showNewsAddPanel(Model model) {
        setModelAttributes(model);
        model.addAttribute("viewType", "news-add");
        model.addAttribute("news", new News());
        return "administration_panel";
    }

    @PostMapping("/admin/news/add/save")
    public String saveNews(@ModelAttribute News news,
                           HttpServletRequest request,
                               Model model) {
        newsService.save(news);

        String referer = request.getHeader("Referer");
        return "redirect:" + referer;
    }

    @GetMapping("/admin/news/search")
    public String getNewsToEdit(@RequestParam(value="title", required=false, defaultValue = "") String title,
                                       Model model) {
        List<News> searchedNews = newsService.findByTitleContaining(title);
        model.addAttribute("searchedNews", searchedNews);
        model.addAttribute("news", new News());
        model.addAttribute("viewType", "news-search");
        return "administration_panel";
    }

    @GetMapping("/admin/news/search/edit")
    public String showNewsToEdit(@RequestParam(value="id", required=false) Long id,
                                     Model model) {
        Optional<News> news = newsService.findById(id);
        model.addAttribute("news", news.get());
        model.addAttribute("viewType", "news-edit");
        return "administration_panel";
    }


    @GetMapping("/admin/users/search")
    public String getUsers(@RequestParam(value="userName", required=false, defaultValue = "") String userName,
                                 Model model) {
        List<User> users = userService.findByUserNameContaining(userName);
        model.addAttribute("users", users);
        model.addAttribute("user", new User());
        model.addAttribute("viewType", "users-search");

        return "administration_panel";
    }

    @GetMapping("/admin/users/search/edit")
    public String showUserToEdit(@RequestParam(value="id", required=false) Long id,
                                 Model model) {
        Optional<User> user = userService.findById(id);
        model.addAttribute("user", user.get());
        model.addAttribute("viewType", "users-edit");
        return "administration_panel";
    }

    @PostMapping("/admin/users/add/save")
    public String saveUser(@ModelAttribute User user,
                           HttpServletRequest request,
                           Model model) {

        String pass = userService.findById(user.getId())
                .get()
                .getPassword();
        user.setPassword(pass);
        userService.save(user);

        String referer = request.getHeader("Referer");
        return "redirect:" + referer;
    }

    @GetMapping("/admin/raport")
    public String generateRaport(Model model){

        List<Attraction> attractions = attractionService.findAll();
        List<User> users = userService.findAll();
        List<News> news = newsService.findAll();
        List<Location> locations = locationService.findAll();

        int attCount = attractions.size();
        int usrCount = users.size();
        int newsCount = news.size();
        int locationCount = locations.size();

        model.addAttribute("attCount", attCount);
        model.addAttribute("usrCount", usrCount);
        model.addAttribute("newsCount", newsCount);
        model.addAttribute("locationCount", locationCount);

        return "raport";
    }

    private void setModelAttributes(Model model) {
        List<Attraction> attractions =  attractionService.findAll();
        List<Location> locations = locationService.findAll();
        List<News> news = newsService.findAll();

        model.addAttribute("allAttractions", attractions);
        model.addAttribute("allLocations", locations);
        model.addAttribute("allNews", news);
        model.addAttribute("user", new User());
    }
}
